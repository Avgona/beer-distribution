package com.avgona.distribution.beer.common;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
public class BaseEntity {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Version
    @Column(name = "VERSION")
    protected Long version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    protected String createdBy;

    @CreatedDate
    @Column(name = "CREATE_DATE")
    protected LocalDateTime createDate;

    @LastModifiedBy
    @Column(name = "LAST_UPDATED_BY")
    protected String lastUpdatedBy;

    @LastModifiedDate
    @Column(name = "LAST_UPDATE_DATE")
    protected LocalDateTime lastUpdateDate;

}
