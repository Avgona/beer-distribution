package com.avgona.distribution.order.controller;

import com.avgona.distribution.order.controller.request.OrderRequest;
import com.avgona.distribution.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/orders")
class OrderController {

    private final OrderService service;

    @Autowired
    OrderController(OrderService service) {
        this.service = service;
    }

    @PostMapping
    public void register(@RequestBody OrderRequest request) {
        service.save(request);
    }
}
