package com.avgona.distribution.order.controller.request;

import lombok.Data;

@Data
public class OrderRequest {
    private String beerName;
    private int amount;
    private int bill;
    private String distributor;
}
