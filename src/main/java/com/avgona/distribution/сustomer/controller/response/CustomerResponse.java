package com.avgona.distribution.сustomer.controller.response;

import lombok.Data;

@Data
public class CustomerResponse {
    private Long id;
    private String username;
    private int cash;
}
