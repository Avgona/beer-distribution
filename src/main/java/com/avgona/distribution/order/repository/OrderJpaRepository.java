package com.avgona.distribution.order.repository;


import com.avgona.distribution.order.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

interface OrderJpaRepository extends JpaRepository<Order, Long> {


}
