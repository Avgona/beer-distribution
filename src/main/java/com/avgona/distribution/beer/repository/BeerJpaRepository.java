package com.avgona.distribution.beer.repository;

import com.avgona.distribution.beer.entity.Beer;
import org.springframework.data.jpa.repository.JpaRepository;

interface BeerJpaRepository extends JpaRepository<Beer, Long> {

}
