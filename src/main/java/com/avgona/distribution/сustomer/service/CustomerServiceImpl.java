package com.avgona.distribution.сustomer.service;

import com.avgona.distribution.сustomer.controller.request.CustomerRequest;
import com.avgona.distribution.сustomer.entity.Customer;
import com.avgona.distribution.сustomer.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository repository;

    @Autowired
    CustomerServiceImpl(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Customer> getAll() {
        return repository.findAll();
    }

    @Override
    public void save(CustomerRequest request) {
        var customer = new Customer();
        repository.save(customer);
    }
}
