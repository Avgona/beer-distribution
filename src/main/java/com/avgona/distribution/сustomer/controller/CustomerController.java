package com.avgona.distribution.сustomer.controller;

import com.avgona.distribution.сustomer.controller.request.CustomerRequest;
import com.avgona.distribution.сustomer.controller.response.CustomerResponse;
import com.avgona.distribution.сustomer.repository.CustomerMapper;
import com.avgona.distribution.сustomer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
class CustomerController {

    private final CustomerService service;
    private final CustomerMapper mapper;

    @Autowired
    CustomerController(CustomerService service, CustomerMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<CustomerResponse> getAll() {
        var customers = service.getAll();
        return mapper.toResponses(customers);
    }

    @PostMapping
    public void register(@RequestBody CustomerRequest request) {
        service.save(request);
    }

}
