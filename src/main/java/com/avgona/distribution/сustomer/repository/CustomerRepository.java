package com.avgona.distribution.сustomer.repository;

import com.avgona.distribution.сustomer.entity.Customer;

import java.util.List;

public interface CustomerRepository {

    List<Customer> findAll();

    void save(Customer customer);
}
