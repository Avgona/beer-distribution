package com.avgona.distribution.beer.entity;

public enum BeerStyle {
    LAGER, PILSNER, STOUT, GOSE, PORTER, ALE, WHEAT, IPA, PALE_ALE, SAISON
}
