package com.avgona.distribution.beer.repository;

import com.avgona.distribution.beer.entity.Beer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
class BeerRepositoryImpl implements BeerRepository {

    private final BeerJpaRepository jpaRepository;

    @Autowired
    BeerRepositoryImpl(BeerJpaRepository jpaRepository) {
        this.jpaRepository = jpaRepository;
    }

    @Override
    public Page<Beer> findAll(Pageable pageable) {
        return jpaRepository.findAll(pageable);
    }

    @Override
    public Beer findById(Long id) {
        return jpaRepository.findById(id).orElseThrow();
    }

    @Override
    public void save(Beer beer) {
        jpaRepository.save(beer);
    }
}
