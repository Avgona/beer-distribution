package com.avgona.distribution.beer.entity;

import com.avgona.distribution.beer.common.BaseEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;


@Getter
@Setter
@ToString
@RequiredArgsConstructor

@Entity
@Table(name = "BEERS")
public class Beer extends BaseEntity {

    @Column(name = "BEER_NAME")
    private String beerName;

    @Column(name = "BEER_STYLE")
    @Enumerated(EnumType.STRING)
    private BeerStyle beerStyle;

    @Column(name = "PRICE")
    private double price;

    public Beer(String beerName, BeerStyle beerStyle, double price) {
        super();
        this.beerName = beerName;
        this.beerStyle = beerStyle;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Beer beer = (Beer) o;
        return id != null && Objects.equals(id, beer.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
