package com.avgona.distribution.beer.service;

import com.avgona.distribution.beer.controller.request.BeerRequest;
import com.avgona.distribution.beer.entity.Beer;
import com.avgona.distribution.beer.repository.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
class BeerServiceImpl implements BeerService{

    private final BeerRepository repository;

    @Autowired
    BeerServiceImpl(BeerRepository repository) {
        this.repository = repository;
    }

    @Override
    public Page<Beer> getAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Beer getById(Long id) {
        return repository.findById(id);
    }

    @Override
    public void save(BeerRequest request) {
        var beer = new Beer(request.getBeerName(), request.getBeerStyle(), request.getPrice());
        repository.save(beer);
    }
}
