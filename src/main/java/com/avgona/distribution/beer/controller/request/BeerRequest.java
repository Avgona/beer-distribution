package com.avgona.distribution.beer.controller.request;

import com.avgona.distribution.beer.entity.BeerStyle;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BeerRequest {

    @NotBlank
    private String beerName;
    @NotBlank
    private BeerStyle beerStyle;
    @Positive
    private double price;
}
