package com.avgona.distribution.сustomer.entity;

import com.avgona.distribution.beer.common.BaseEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@ToString
@RequiredArgsConstructor

@Entity
@Table(name = "users")
public class Customer extends BaseEntity {

    @Column(name = "NAME")
    private String username;

    @Column(name = "CASH")
    private int cash;

    public Customer(String username, int cash) {
        super();
        this.username = username;
        this.cash = cash;
    }
}
