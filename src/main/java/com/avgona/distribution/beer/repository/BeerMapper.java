package com.avgona.distribution.beer.repository;

import com.avgona.distribution.beer.controller.request.BeerRequest;
import com.avgona.distribution.beer.entity.Beer;
import org.mapstruct.Mapper;

@Mapper
public interface BeerMapper {

    Beer toBeer(BeerRequest request);
}
