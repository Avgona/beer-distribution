package com.avgona.distribution.beer.repository;

import com.avgona.distribution.beer.entity.Beer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BeerRepository {

    Page<Beer> findAll(Pageable pageable);

    Beer findById(Long id);

    void save(Beer beer);

}
