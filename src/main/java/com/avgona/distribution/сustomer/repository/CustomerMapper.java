package com.avgona.distribution.сustomer.repository;

import com.avgona.distribution.сustomer.controller.response.CustomerResponse;
import com.avgona.distribution.сustomer.entity.Customer;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface CustomerMapper {

    List<CustomerResponse> toResponses(List<Customer> customer);

}
