package com.avgona.distribution.order.entity;

import com.avgona.distribution.beer.common.BaseEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@ToString
@RequiredArgsConstructor

@Entity
@Table(name = "orders")
public class Order extends BaseEntity {

    private String beerName;
    private int amount;
    private int bill;
    private String distributor;


    public Order(String beerName, int amount, int bill, String distributor) {
        super();
        this.beerName = beerName;
        this.amount = amount;
        this.bill = bill;
        this.distributor = distributor;
    }
}
