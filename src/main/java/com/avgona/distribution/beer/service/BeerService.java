package com.avgona.distribution.beer.service;

import com.avgona.distribution.beer.controller.request.BeerRequest;
import com.avgona.distribution.beer.entity.Beer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BeerService {

    Page<Beer> getAll(Pageable pageable);

    Beer getById(Long id);

    void save(BeerRequest request);

}
