package com.avgona.distribution.beer.controller;

import com.avgona.distribution.beer.controller.request.BeerRequest;
import com.avgona.distribution.beer.entity.Beer;
import com.avgona.distribution.beer.service.BeerService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Validated
@RestController
@RequestMapping("/beer")
class BeerController {

    private final BeerService service;

    @Autowired
    BeerController(BeerService service) {
        this.service = service;
    }

    @GetMapping
    public Page<Beer> getAll(@PageableDefault(sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
        return service.getAll(pageable);
    }
    @GetMapping("/{id}")
    public Beer getById(@NotBlank @PathVariable("id") Long id) {
        return service.getById(id);
    }

    @PostMapping
    public void create(@Valid @RequestBody BeerRequest request){
        service.save(request);
    }
}
