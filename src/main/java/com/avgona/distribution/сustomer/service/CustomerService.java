package com.avgona.distribution.сustomer.service;

import com.avgona.distribution.сustomer.controller.request.CustomerRequest;
import com.avgona.distribution.сustomer.entity.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getAll();

    void save(CustomerRequest request);
}
