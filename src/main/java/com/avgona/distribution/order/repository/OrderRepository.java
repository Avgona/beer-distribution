package com.avgona.distribution.order.repository;

import com.avgona.distribution.order.entity.Order;

public interface OrderRepository {

    void save(Order order);

}
