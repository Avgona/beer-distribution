package com.avgona.distribution.beer.common.exceptionHandler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public void handlerEmptyRequestBody(HttpMessageNotReadableException ex){
        log.error("The system didn't get any payload from request");
    }
}
