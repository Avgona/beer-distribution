package com.avgona.distribution.сustomer.repository;


import com.avgona.distribution.сustomer.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

interface CustomerJpaRepository extends JpaRepository<Customer, Long> {


}
