package com.avgona.distribution.order.service;

import com.avgona.distribution.order.controller.request.OrderRequest;
import com.avgona.distribution.order.entity.Order;
import com.avgona.distribution.order.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
class OrderServiceImpl implements OrderService {

    private final OrderRepository repository;

    @Autowired
    OrderServiceImpl(OrderRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(OrderRequest request) {
        var order = new Order();
        repository.save(order);
    }
}
