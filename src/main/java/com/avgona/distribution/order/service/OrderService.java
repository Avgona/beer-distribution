package com.avgona.distribution.order.service;

import com.avgona.distribution.order.controller.request.OrderRequest;

import java.util.List;

public interface OrderService {

    void save(OrderRequest request);
}
