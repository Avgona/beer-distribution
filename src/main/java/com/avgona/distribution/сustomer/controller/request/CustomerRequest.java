package com.avgona.distribution.сustomer.controller.request;

import lombok.Data;

import javax.persistence.Column;

@Data
public class CustomerRequest {
    private String username;
    private int cash;
}
